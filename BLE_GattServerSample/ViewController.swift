//
//  ViewController.swift
//  BLE_GATTServer
//
//  Created by Padma Pravallika Gottumukkala on 09/12/20.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController,CBPeripheralManagerDelegate {
    
    

    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var readValLbl: UILabel!
    @IBOutlet weak var writeValLbl: UILabel!
    
//    private var service: CBUUID!
//    private let value = "b464c0e4-3e9d-11eb-b378-0242ac130002"
   
    
    private var peripheralManager : CBPeripheralManager!
    private var centralManager : CBCentralManager!
    private var centralCharacteristic: CBCharacteristic?

    override func viewDidLoad() {
        super.viewDidLoad()
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
       // centralManager = CBCentralManager(delegate: self, queue: nil)
        // Do any additional setup after loading the view.
    }
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
    switch peripheral.state {
        case .unknown:
            print("Bluetooth Device is UNKNOWN")
        case .unsupported:
            print("Bluetooth Device is UNSUPPORTED")
        case .unauthorized:
            print("Bluetooth Device is UNAUTHORIZED")
        case .resetting:
            print("Bluetooth Device is RESETTING")
        case .poweredOff:
            print("Bluetooth Device is POWERED OFF")
        case .poweredOn:
            print("Bluetooth Device is POWERED ON")
            addServices()
        @unknown default:
            print("Unknown State")
        }
    }
    func addServices() {
        
        let valueData = kBLEService_UUID.data(using: .utf8)
        
        // 1. Create instance of CBMutableCharcateristic
        let myCharacteristic1 = CBMutableCharacteristic(type: BLE_Characteristic_uuid_Rx, properties: [.read], value: valueData, permissions: [.readable])
        let myCharacteristic2 = CBMutableCharacteristic(type: BLE_Characteristic_uuid_Tx, properties: [.read,.write,.notify], value: nil, permissions: [.readable,.writeable])
    
        // 2. Create instance of CBMutableService
        let myService = CBMutableService(type: BLEService_UUID, primary: true)
        
        // 3. Add characteristics to the service
        myService.characteristics = [myCharacteristic1, myCharacteristic2]
        
        // 4. Add service to peripheralManager
        peripheralManager.add(myService)
        
        // 5. Start advertising
        startAdvertising()
       
    }
    
    
    func startAdvertising() {
        messageLbl.text = "Advertising Data"
        peripheralManager.startAdvertising([CBAdvertisementDataLocalNameKey : "LIVMOR Watch Service", CBAdvertisementDataServiceUUIDsKey : [BLEService_UUID]]) //advertise with service UUID
        print("Started Advertising")
    }
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
      if error != nil {
        print("Error connecting to peripheral: \(String(describing: error?.localizedDescription))")
        return
      }
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        messageLbl.text = "Data getting Read"
       // readValLbl.text = value
        
       
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        
        messageLbl.text = "Writing Data"
       
        if let value = requests.first?.value {
            writeValLbl.text = value.hexEncodedString()
            
        }
    }
    
    func peripheralManager(
        peripheral: CBPeripheralManager,
        central: CBCentral,
        didSubscribeToCharacteristic characteristic: CBCharacteristic)
    {
        //print("subscribed centrals: \(central.subscribedCentrals)")
    }
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        // Check subscription here
        print("isNotifying: \(characteristic.isNotifying)")
    }

    
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}
